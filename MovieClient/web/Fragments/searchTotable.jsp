<%-- 
    Document   : normalSearch
    Created on : Nov 5, 2016, 6:14:18 PM
    Author     : nuyuyii
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <title>Search Page</title>       

    </head>
    <body>
        
        <%-- start web service invocation --%>    
        <div class="page1-box2 centxt">
            <%
            try {
                String category_name = request.getParameter("category");
                String search_name = "all";
                //select all 
                if(request.getParameter("search")!=""){
                   search_name = request.getParameter("search");                
                }
                serverpack.MvWebService_Service service = new serverpack.MvWebService_Service();
                serverpack.MvWebService port = service.getMvWebServicePort();
                // TODO initialize WS operation arguments here
                java.lang.String category = category_name;
                java.lang.String search = search_name;
                // TODO process result here
                java.lang.String result = port.searchQue3(category, search);
                out.println("<h3>Category = " + category_name);
                out.println("</h3><h3><br>Search = " + search_name+"</h3>");
            %>
        </div>
        <table table align="center" border="1" style="border-color:#778899;border-style: solid;border-width: 8px;">
            <tr bgcolor="#EE82EE">
                    <th>NodeID</th>
                    <th>Title</th>
                    <th>Year</th>
                    <th>Type</th>
                    <th>Time</th>
                    <th>Director</th>
                    <th>Shutter</th>
                    <th>Resolution</th>
                    <th>Delete</th>
                    <th>Edit</th>

            </tr>
        <%
            String show = result;
            
            for (String s: show.split("---")){                
                String[] show_film = s.split("--",9);
                String[] NodeID = s.split("::",3);
         %>
    
           
               <%if (NodeID[0].equals("Selete Search Pls.")){
                      out.println("Selete Search Pls.");
                  }else{
               
               %>
                <tr bgcolor="#F5F5F5">
                    <td > <%=NodeID[1]%></td>             
                    <td ><%=show_film[1]%></td>
                    <td ><%=show_film[2]%></td>
                    <td ><%=show_film[3]%></td>
                    <td ><%=show_film[4]%></td>
                    <td ><%=show_film[5]%></td>
                    <td ><%=show_film[6]%></td>
                    <td ><%=show_film[7]%></td>
                    <td>
                        <form action="Delete.jsp" method="POST">
                            <input type="hidden" name="number" value="<%=show_film[7]%>" />
                            <input class="button1" name="act" type="submit" value="Delete" />
                        </form>
                    </td><td>
                        <form action="Edit.jsp" method="POST">
                            <input type="hidden" name="number" value="<%=show_film[7]%>" />
                            <input class="button1" name="act" type="submit" value="Edit" />
                        </form>
                    </td>
                </tr>
               <% }} %>
               
                </table>

        <%  
        
        //out.println(show);
        } catch (Exception ex) {
              // TODO handle custom exceptions here
        }
        %>

        <%-- end web service invocation --%>
    </body>
</html>
